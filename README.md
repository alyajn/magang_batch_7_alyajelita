 fungsi penggunaan git pada project

 Working with projects ALL TIERS
Most work in GitLab is done in a project. Files and code are saved in projects, and most features are in the scope of projects.

Explore projects
You can explore other popular projects available on GitLab. To explore projects:

On the top bar, select Menu > Project.
Select Explore projects.
GitLab displays a list of projects, sorted by last updated date. To view projects with the most stars, click Most stars. To view projects with the largest number of comments in the past month, click Trending.

By default, /explore is visible to unauthenticated users. However, if the Public visibility level is restricted, /explore is visible only to signed-in users.
Create a project
To create a project in GitLab:

In your dashboard, click the green New project button or use the plus icon in the navigation bar. This opens the New project page.
On the New project page, choose if you want to:
Create a blank project.
Create a project using one of the available project templates.
Import a project from a different repository, if enabled on your GitLab instance. Contact your GitLab administrator if this is unavailable.
Run CI/CD pipelines for external repositories. 
For a list of words that can’t be used as project names see Reserved project and group names.
Blank projects
To create a new blank project on the New project page:

Click Create blank project
Provide the following information:
The name of your project in the Project name field. You can’t use special characters, but you can use spaces, hyphens, underscores, or even emoji. When adding the name, the Project slug auto populates. The slug is what the GitLab instance uses as the URL path to the project. If you want a different slug, input the project name first, then change the slug after.
The path to your project in the Project slug field. This is the URL path for your project that the GitLab instance uses. If the Project name is blank, it auto populates when you fill in the Project slug.
The Project description (optional) field enables you to enter a description for your project’s dashboard, which helps others understand what your project is about. Though it’s not required, it’s a good idea to fill this in.
Changing the Visibility Level modifies the project’s viewing and access rights for users.
Selecting the Initialize repository with a README option creates a README file so that the Git repository is initialized, has a default branch, and can be cloned.
Click Create project.
Project templates
Project templates can pre-populate a new project with the necessary files to get you started quickly.

There are two main types of project templates:

Built-in templates, sourced from the following groups:
project-templates
pages
Custom project templates, for custom templates configured by GitLab administrators and users.
Built-in templates
Built-in templates are project templates that are:

Developed and maintained in the project-templates and pages groups.
Released with GitLab.
Anyone can contribute a built-in template by following these steps.
To use a built-in template on the New project page:

Click Create from template
Select the Built-in tab.
From the list of available built-in templates, click the:
Preview button to look at the template source itself.
Use template button to start creating the project.
Finish creating the project by filling out the project’s details. The process is the same as creating a blank project.
Enterprise templates ULTIMATE
GitLab is developing Enterprise templates to help you streamline audit management with selected regulatory standards. These templates automatically import issues that correspond to each regulatory requirement.

To create a new project with an Enterprise template, on the New project page:

Click Create from template
Select the Built-in tab.
From the list of available built-in Enterprise templates, click the:
Preview button to look at the template source itself.
Use template button to start creating the project.
Finish creating the project by filling out the project’s details. The process is the same as creating a blank project.
Available Enterprise templates include:

HIPAA Audit Protocol template (introduced in GitLab 12.10)
You can improve the existing built-in templates or contribute new ones in the project-templates and pages groups by following these steps.
Custom project templates PREMIUM
Introduced in GitLab Premium 11.2.

Creating new projects based on custom project templates is a convenient option for quickly starting projects.

Custom projects are available at the instance-level from the Instance tab, or at the group-level from the Group tab, on the Create from template page.

To use a custom project template on the New project page:

Click Create from template
Select the Instance tab or the Group tab.
From the list of available custom templates, click the:
Preview button to look at the template source itself.
Use template button to start creating the project.
Finish creating the project by filling out the project’s details. The process is the same as creating a blank project.
Push to create a new project
Introduced in GitLab 10.5.

When you create a new repository locally, you don’t have to sign in to the GitLab interface to create a project and clone its repository. You can directly push your new repository to GitLab, which creates your new project without leaving your terminal.

To push a new project:

Identify the namespace you want to add the new project to, as you need this information in a future step. To determine if you have permission to create new projects in a namespace, view the group’s page in a web browser and confirm the page displays a New project button.

As project creation permissions can have many factors, contact your GitLab administrator if you’re unsure.
If you want to push using SSH, ensure you have created a SSH key and added it to your GitLab account.
Push with one of the following methods. Replace gitlab.example.com with the domain name of the machine that hosts your Git repository, namespace with the name of your namespace, and myproject with the name of your new project:
To push with SSH: git push --set-upstream git@gitlab.example.com:namespace/myproject.git master
To push with HTTPS: git push --set-upstream https://gitlab.example.com/namespace/myproject.git master Optional: to export existing repository tags, append the --tags flag to your git push command.
When the push completes, GitLab displays a message:

remote: The private project namespace/myproject was created.

(Optional) To configure the remote, alter the command git remote add origin https://gitlab.example.com/namespace/myproject.git to match your namespace and project names.
You can view your new project at https://gitlab.example.com/namespace/myproject. Your project’s visibility is set to Private by default, but you can change it in your project’s settings).

Fork a project
A fork is a copy of an original repository that you put in another namespace where you can experiment and apply changes that you can later decide whether or not to share, without affecting the original project.

It takes just a few steps to fork a project in GitLab.

Star a project
You can star a project to make it easier to find projects you frequently use. The number of stars a project has can indicate its popularity.

To star a project:

Go to the home page of the project you want to star.
In the upper right corner of the page, click Star.
To view your starred projects:

On the top bar, select Menu > Project.
Select Starred Projects.
GitLab displays information about your starred projects, including:

Project description, including name, description, and icon
Number of times this project has been starred
Number of times this project has been forked
Number of open merge requests
Number of open issues
Delete a project
To delete a project, first navigate to the home page for that project.

Navigate to Settings > General.
Expand the Advanced section.
Scroll down to the Delete project section.
Click Delete project
Confirm this action by typing in the expected text.
Projects in personal namespaces are deleted immediately on request. For information on delayed deletion of projects in a group, please see Enable delayed project removal.

Project settings
Set the project’s visibility level and the access levels to its various pages and perform actions like archiving, renaming or transferring a project.

Read through the documentation on project settings.

Project activity
To view the activity of a project:

On the left sidebar, select Project information > Activity.
Select a tab to view All the activity, or to filter it by any of these criteria:
Push events
Merge events
Issue events
Comments
Team
Wiki
Leave a project
Leave project only displays on the project’s dashboard when a project is part of a group (under a group namespace). If you choose to leave a project you are no longer a project member, and cannot contribute.

Use your project as a Go package
Any project can be used as a Go package. GitLab responds correctly to go get and godoc.org discovery requests, including the go-import and go-source meta tags.

Private projects, including projects in subgroups, can be used as a Go package, but may require configuration to work correctly. GitLab responds correctly to go get discovery requests for projects that are not in subgroups, regardless of authentication or authorization. Authentication is required to use a private project in a subgroup as a Go package. Otherwise, GitLab truncates the path for private projects in subgroups to the first two segments, causing go get to fail.

GitLab implements its own Go proxy. This feature must be enabled by an administrator and requires additional configuration. See GitLab Go Proxy.

Disable Go module features for private projects
In Go 1.12 and later, Go queries module proxies and checksum databases in the process of fetching a module. This can be selectively disabled with GOPRIVATE (disable both), GONOPROXY (disable proxy queries), and GONOSUMDB (disable checksum queries).

GOPRIVATE, GONOPROXY, and GONOSUMDB are comma-separated lists of Go modules and Go module prefixes. For example, GOPRIVATE=gitlab.example.com/my/private/project disables queries for that one project, but GOPRIVATE=gitlab.example.com disables queries for all projects on GitLab.com. Go does not query module proxies if the module name or a prefix of it appears in GOPRIVATE or GONOPROXY. Go does not query checksum databases if the module name or a prefix of it appears in GONOPRIVATE or GONOSUMDB.

Authenticate Go requests
To authenticate requests to private projects made by Go, use a .netrc file and a personal access token in the password field. This only works if your GitLab instance can be accessed with HTTPS. The go command does not transmit credentials over insecure connections. This authenticates all HTTPS requests made directly by Go, but does not authenticate requests made through Git.

For example:

machine gitlab.example.com
login <gitlab_user_name>
password <personal_access_token>

On Windows, Go reads ~/_netrc instead of ~/.netrc.
Authenticate Git fetches
If a module cannot be fetched from a proxy, Go falls back to using Git (for GitLab projects). Git uses .netrc to authenticate requests. You can also configure Git to either:

Embed specific credentials in the request URL.
Use SSH instead of HTTPS, as Go always uses HTTPS to fetch Git repositories.
# Embed credentials in any request to GitLab.com:
git config --global url."https://${user}:${personal_access_token}@gitlab.example.com".insteadOf "https://gitlab.example.com"

# Use SSH instead of HTTPS:
git config --global url."git@gitlab.example.com".insteadOf "https://gitlab.example.com"

Fetch Go modules from Geo secondary sites
As Go modules are stored in Git repositories, you can use the Geo feature that allows Git repositories to be accessed on the secondary Geo servers.

In the following examples, the primary’s site domain name is gitlab.example.com, and the secondary’s is gitlab-secondary.example.com.

go get will initially generate some HTTP traffic to the primary, but when the module download commences, the insteadOf configuration sends the traffic to the secondary.

Use SSH to access the Geo secondary
To fetch Go modules from the secondary using SSH:

Reconfigure Git on the client to send traffic for the primary to the secondary:

git config --global url."git@gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"
git config --global url."git@gitlab-secondary.example.com".insteadOf "http://gitlab.example.com"

Ensure the client is set up for SSH access to GitLab repositories. This can be tested on the primary, and GitLab will replicate the public key to the secondary.

Use HTTP to access the Geo secondary
Using HTTP to fetch Go modules does not work with CI/CD job tokens, only with persistent access tokens that are replicated to the secondary.

To fetch Go modules from the secondary using HTTP:

Put in place a Git insteadOf redirect on the client:

git config --global url."https://gitlab-secondary.example.com".insteadOf "https://gitlab.example.com"

Generate a personal access token and provide those credentials in the client’s ~/.netrc file:

machine gitlab.example.com login USERNAME password TOKEN
machine gitlab-secondary.example.com login USERNAME password TOKEN

Access project page with project ID
Introduced in GitLab 11.8.

To quickly access a project from the GitLab UI using the project ID, visit the /projects/:id URL in your browser or other tool accessing the project.

Project’s landing page
The project’s landing page shows different information depending on the project’s visibility settings and user permissions.

For public projects, and to members of internal and private projects with permissions to view the project’s code:

The content of a README or an index file is displayed (if any), followed by the list of directories in the project’s repository.
If the project doesn’t contain either of these files, the visitor sees the list of files and directories of the repository.
